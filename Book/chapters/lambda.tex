% -*- latex -*-
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%
%%%% This TeX file is part of the course
%%%% Introduction to Scientific Programming in C++/Fortran2003
%%%% copyright 2017-2022 Victor Eijkhout eijkhout@tacc.utexas.edu
%%%%
%%%% lambda.tex : closures
%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The mechanism of \indextermbusp{lambda}{expression} (added in
\cppstandard{11}) makes
dynamic definition of functions possible.

\begin{block}{A simple example}
  \label{sl:lambda-example}
  You can define a function and apply it:
\begin{lstlisting}
double sum(float x,float y) { return x+y; }
cout << sum( 1.2, 3.4 );
\end{lstlisting}
or you can apply the function recipe directly:
\snippetwithoutput{lambdaexp}{func}{lambdadirect}
\end{block}

This example is of course fairly pointless,
but it illustrates the syntax of a lambda function:
\begin{lstlisting}
[capture] ( inputs ) -> outtype { definition };
[capture] ( inputs ) { definition };  
\end{lstlisting}

\begin{itemize}
\item The square brackets, in this case, but not in general, empty,
  are the \indextermdef{capture} part;
\item then follows the usual argument list;
\item with a stylized arrow you can indicate the return type,
  but this is optional if the compiler can figure it out by itself;
\item and finally the usual function body, include \lstinline{return} statement
  for non-void functions.
\end{itemize}

\begin{remark}
  Lambda functions are sometimes called \indextermp{closure},
  but this term has a technical meaning in programming language theory,
  that only partly coincides with C++ lambda functions.
\end{remark}

For a slightly more useful example,
we can assign the lambda function to a variable, and repeatedly apply it.

\begin{block}{Assign lambda to variable}
  \label{sl:lambdavar}
\snippetwithoutput{lambdavar}{func}{lambdavar}
\begin{itemize}
\item This is a variable declaration.
\item Uses \lstinline+auto+ for technical reasons; see later.
\end{itemize}
Return type could have been ommitted:
\begin{lstlisting}
    auto summing = 
    [] (float x,float y) { return x+y; };
\end{lstlisting}
\end{block}

\begin{slide}{Lambda syntax}
  \label{sl:lambda-syntax}
\begin{lstlisting}
[capture] ( inputs ) -> outtype { definition };
[capture] ( inputs ) { definition };  
\end{lstlisting}
  \begin{itemize}
  \item The square brackets are how you recognize a lambda;\\
    we will get to the `capture' later.
  \item Inputs: like function parameters
  \item Result type specification \lstinline+-> outtype+:\\
    can be omitted if compiler can deduce it;
  \item Definition: function body.
  \end{itemize}
\end{slide}

\begin{exercise}
  Do exercise~\ref{ex:newton-functions} of the zero finding project.
\end{exercise}

\Level 0 {Passing lambdas around}
\label{sec:lambdaauto}

Above, when we assigned a lambda function to a variable,
we used \lstinline{auto} for the type.
The reason for this is that each lambda function gets its own
unique type, that is dynamically generated.
But that makes it hard to pass that variable to a function.

Suppose we want to pass a lambda expression to a function:
\begin{lstlisting}
int main() {
    apply_to_5( [] (int i) { cout << i+1; } );
\end{lstlisting}
What type do we use for the function parameter?
\begin{lstlisting}
void apply_to_5( /* what type are we giving? */ f ) {
    f(5);
}
\end{lstlisting}
Since the type of the lambda expression is dynamically generated,
we can not specify that type in the function header.

The way out is to use the \lstinline{functional} header:
\begin{lstlisting}
#include <functional>
using std::function;
\end{lstlisting}
With this, you can declare parameters by their signature:

\snippetwithoutput{lambdapass}{func}{lambdapass}

\begin{exercise}
  Do exercise~\ref{ex:newton-function-ptr} of the zero-finding project.
\end{exercise}

\Level 1 {Lambda members of classes}

The fact that lambda function have a dynamically generated type
also makes it hard to store a lambda in an object.
Again we use \lstinline{std::}\indexc{function}.

\begin{block}{Lambda in object}
  \label{sl:lambda-class}
  %
  A set of integers, with a test on which ones can be admitted:
  \begin{multicols}{2}
    \verbatimsnippet{lambdaclass}
  \end{multicols}
\end{block}

\begin{block}{Illustration}
  \label{sl:lambda-classed}
  \snippetwithoutput{lambdaclassed}{func}{lambdafun}
\end{block}

\Level 0 {Captures}

A non-trivial use of lambdas uses the \indexterm{capture} to fix one argument of a
function.
Let's say we want a function that computes exponentials for some fixed
exponent value.
We take the \indexc{pow} function from the 
\indextermtt{cmath} header:
\begin{lstlisting}
pow( x,exponent );
\end{lstlisting}
and fix the exponent:
%
\verbatimsnippet{lambdacapt}
%
Now \n{powerfunction} is a function of one argument, which computes
that argument to a fixed power.

\begin{slide}{Capture parameter}
  \label{sl:lambda-capture}
  Capture value and reduce number of arguments:
  %
  \verbatimsnippet{lambdacapt}

  Now \n{powerfive} is a function of one argument, which computes
  that argument to a fixed power.
  %
  \snippetwithoutput{lambdapowercall}{func}{lambdait}
\end{slide}

\begin{exercise}
  Do exercises \ref{ex:newton-capture-root} and~\ref{ex:newton-capture-diff}
  of the zero-finding project.
\end{exercise}

\Level 0 {Capture by reference}

Normally, captured variables are copied by value.
\snippetwithoutput{lambdavalue}{func}{lambdavalue}

Attempting to change the captured variable doesn't even compile:
\begin{lstlisting}
auto f = // WRONG DOES NOT COMPILE
    [x] ( float &y ) -> void {
        x *= 2; y += x; };
\end{lstlisting}

If you do want to alter the captured parameter,
pass it by reference:
\snippetwithoutput{lambdareference}{func}{lambdareference}

\begin{slide}{Capture by value}
  \label{sl:lambda-val-val}
Normal capture is by value:
\snippetwithoutput{lambdavalue}{func}{lambdavalue}
\end{slide}

\begin{slide}{Capture by value/reference}
  \label{sl:lambda-ref-ref}
  Capture by reference:
\snippetwithoutput{lambdareference}{func}{lambdareference}
\end{slide}

Capturing by reference can for instance be useful
if you are performing some sort of reduction.
The capture is then the reduction variable,
and the numbers to be reduced come in as function parameter
to the lambda expression.

In this example we count how many of the input values
test true under a certain function~\lstinline{f}:

\begin{block}{Capture a reduction variable}
  \label{sl:capture-count}
This mechanism is useful 
\begin{lstlisting}
int count=0;
auto count_if_f = [&count] (int i) {
      if (f(i)) count++; }
for ( int i : int_data )
  count_if_f(i);
cout << "We counted: " << count;
\end{lstlisting}
\end{block}

\Level 0 {More}

\Level 1 {Making lambda stateful}

\snippetwithoutput[printeach]{counteach}{stl}{counteach}

\begin{slide}{Capture by reference}
  \label{sl:lambda-count}
  Capture variables are normally by value,
  use ampersand for reference.
  This is often used in \lstinline{algorithm} header.
\snippetwithoutput[printeach]{counteach}{stl}{counteach}
\end{slide}

Lambda functions are normally stateless:

\snippetwithoutput{lambdanonmutable}{func}{nonmutable}

but with the \indexcdef{mutable} keyword you can
make them stateful:

\snippetwithoutput{lambdayesmutable}{func}{yesmutable}

Here is a nifty application: printing a list of numbers,
separated by commas, but without trailing comma:
%
\snippetwithoutput{mutablecomma}{func}{lambdaexch}

\Level 1 {Generic lambdas}
\label{sec:lambda-generic}

The \lstinline{auto} keyword can be used, almost abused, for
\indextermsubp{generic}{lambda}:
\begin{lstlisting}
auto compare = [] (auto a,auto b) { return a<b; };
\end{lstlisting}
Here the return type is clear, but the input types are generic.

\Level 1 {Algorithms}

The \indextermtt{algorithm} header (section~\ref{sec:algorithm})
contains a number of functions that
naturally use lambdas. For instance, \indexc{any_of} can test
whether any element of a \lstinline{vector} satisfies a condition.
You can then use a lambda to specify the \lstinline{bool} function
that tests the condition.

\Level 1 {C-style function pointers}

The C language had a --~somewhat confusing~-- notation
for function pointers.
If you need to interface with code that uses them,
it is possible to use lambda functions to an extent:
lambdas without captures can be converted to a function pointer.

\snippetwithoutput{lambdacptr}{func}{lambdacptr}
