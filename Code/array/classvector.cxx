/****************************************************************
 ****
 **** This file belongs with the course
 **** Introduction to Scientific Programming in C++/Fortran2003
 **** copyright 2016-2021 Victor Eijkhout eijkhout@tacc.utexas.edu
 ****
 **** classvector.cxx : use of vector in class
 ****
 ****************************************************************/

#include <iostream>
using std::cin, std::cout;

#include <vector>
using std::vector;

class vectorclass {
private:
  vector<int> internal;
public:
  vectorclass(int l) {
    for (int i=0; i<l; i++)
      internal.push_back(0);
  };
  int size() { return internal.size(); };
};

int main() {
  int array_length;
  cout << "How many elements? ";
  cin >> array_length;

  vector<double> my_array(array_length);
  cout << "my array has length " << my_array.size() << '\n';

  vector<double> my_reserve; my_reserve.reserve(array_length);
  cout << "my reserve has length " << my_reserve.size() << '\n';
  
  vectorclass vc(array_length);
  cout << "my class has length " << vc.size() << '\n';
  return 0;
}
