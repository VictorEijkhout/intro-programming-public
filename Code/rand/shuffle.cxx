/****************************************************************
 ****
 **** This file belongs with the course
 **** Introduction to Scientific Programming in C++/Fortran2003
 **** copyright 2020 Victor Eijkhout eijkhout@tacc.utexas.edu
 ****
 **** the C++ permutation generator
 ****
 **** WRONG
 ****
 ****************************************************************/

#include <iostream>
using std::cin;
using std::cout;

#include <random>

int main() {

  std::random_device r;
  auto shuffle30 = std::shuffle_order_engine<std::default_random_engine,30>();

  for (int i=0; i<30; i++) {
    auto irand = shuffle30();
    cout << irand << " ";
  }
  cout << '\n';

  return 0;
}
