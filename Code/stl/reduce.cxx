/****************************************************************
 ****
 **** This file belongs with the course
 **** Introduction to Scientific Programming in C++/Fortran2003
 **** copyright 2019 Victor Eijkhout eijkhout@tacc.utexas.edu
 ****
 **** reduce.cxx : use of reductions
 ****
 ****************************************************************/


#include <iostream>
using std::cin;
using std::cout;

#include <vector>
using std::vector;

#include <numeric>
using std::accumulate;
using std::multiplies;

int main() {

  {
    cout << "Accumulate .." << '\n';
    //codesnippet vecaccumulate
    vector<int> v{1,3,5,7};
    auto first = v.begin();
    auto last  = v.end();
    auto sum = accumulate(first,last,0);
    cout << "sum: " << sum << '\n';
    //codesnippet end
    cout << ".. accumulate" << '\n';
  }

  {
    cout << "Product .." << '\n';
    //codesnippet vecproduct
    vector<int> v{1,3,5,7};
    auto first = v.begin();
    auto last  = v.end();
    first++; last--;
    auto product =
      accumulate
        (first,last,2,multiplies<>());
    cout << "product: " << product << '\n';
    //codesnippet end
    cout << ".. product" << '\n';
  }

  return 0;
}

