/****************************************************************
 ****
 **** This file belongs with the course
 **** Introduction to Scientific Programming in C++/Fortran2003
 **** copyright 2017-2021 Victor Eijkhout eijkhout@tacc.utexas.edu
 ****
 **** testzero.cxx : unittests for root finding
 ****
 ****************************************************************/

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <iomanip>
using std::setw;

#include <vector>
using std::vector;

#include "findzerolib.h"

#define CATCH_CONFIG_MAIN
#include "catch2/catch_all.hpp"

//codesnippet rootcatchodd
TEST_CASE( "polynomial degree" ) {
  vector<double> second{2,0,1}; // 2x^2 + 1
  REQUIRE( not is_odd(second) );
  vector<double> third{3,2,0,1}; // 3x^3 + 2x^2 + 1
  REQUIRE( is_odd(third) );
}
//codesnippet end

TEST_CASE( "polynomial evaluation" ) {
  //codesnippet rootcatcheval
  // correct interpretation: 2x^2 + 1
  vector<double> second{2,0,1}; 
  REQUIRE( evaluate_at(second,2) == Catch::Approx(9) );
  // wrong interpretation: 1x^2 + 2
  REQUIRE( evaluate_at(second,2) != Catch::Approx(6) );
  //codesnippet end
  REQUIRE( evaluate_at(second,2) == Catch::Approx(9) );
  vector<double> third{3,2,0,1}; // 3x^3 + 2x^2 + 1
  REQUIRE( evaluate_at(third,0) == Catch::Approx(1) );
}

TEST_CASE( "outer bounds" ) {
  double left{10},right{11};
  //codesnippet rootcatchouter
  right = left+1;
  vector<double> second{2,0,1}; // 2x^2 + 1
  REQUIRE_THROWS( find_outer(second,left,right) );
  vector<double> third{3,2,0,1}; // 3x^3 + 2x^2 + 1
  REQUIRE_NOTHROW( find_outer(third,left,right) );
  REQUIRE( left<right );
  //codesnippet end
  double
    leftval = evaluate_at(third,left),
    rightval = evaluate_at(third,right);
  REQUIRE( leftval*rightval<=0 );
}
